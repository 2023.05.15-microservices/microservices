#pragma once

#include <cstdlib>
#include <string>
// #include <odb/core.hxx>

#pragma db object
struct Person {
    #pragma db id auto
    int id;
    std::string name;
    int age;
};
