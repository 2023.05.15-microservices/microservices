
#include "lib/json.hpp"
#include "models/person.h"

using json = nlohmann::json;

void to_json(json& j, const Person& p) {
    j = json{{"id", p.id}, {"name", p.name}, {"age", p.age}};
}

void from_json(const json& j, Person& p) {
    try {
        j.at("id").get_to(p.id);
    } catch (json::out_of_range exc) {
        p.id = 0;
    };
    j.at("name").get_to(p.name);
    j.at("age").get_to(p.age);
}