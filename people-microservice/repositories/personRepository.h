#pragma once

#include <vector>
#include <optional>
#include "../models/person.h"

class PersonRepository {
    public:
    const virtual std::vector<Person> findAll() = 0;
    virtual std::optional<Person> findById(int id) = 0;
    virtual Person& save(Person& p) = 0;
    virtual void update(Person& p) = 0;
    virtual void deleteById(int id) = 0;
};