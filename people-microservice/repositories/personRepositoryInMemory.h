#pragma once

#include "../exceptions/responseStatusException.h"
#include "personRepository.h"

class PersonRepositoryInMemory : public PersonRepository {
   private:
    std::vector<Person> people = {{1, "andre", 40},
                                  {2, "pierre alexandre", 30},
                                  {3, "julien", 28},
                                  {4, "pierre yves", 3}};

    std::vector<Person>::iterator iteratorFromId(int id) {
        return std::find_if(people.begin(), people.end(),
                            [&id](Person p) { return p.id == id; });
    }

    static inline int nextId = 5;

   public:
    const std::vector<Person> findAll() { return people; };

    std::optional<Person> findById(int id) {
        auto result = iteratorFromId(id);
        return (result == people.end()) ? std::nullopt
                                        : std::optional<Person>(*result);
    };

    Person& save(Person& p) {
        p.id = nextId++;
        people.push_back(p);
        return p;
    };

    void update(Person& p) {
        auto result = iteratorFromId(p.id);
        if (result != people.end())
            *result = p;
        else
            throw ResponseStatusException(
                400, "no person with id " + std::to_string(p.id) + " exist");
    };

    void deleteById(int id) {
        auto result = iteratorFromId(id);
        if (result == people.end()) {
            throw ResponseStatusException(
                400, "no person with id " + std::to_string(id) + " exist");
        } else {
            people.erase(result);
        }
    };
};