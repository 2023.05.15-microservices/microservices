#include <algorithm>
#include <vector>
#include "lib/httplib.h"
#include "lib/json.hpp"
#include "models/person.h"
#include "exceptions/responseStatusException.h"
#include "repositories/personRepository.h"
#include "repositories/personRepositoryInMemory.h"
#include "serializers.h"

using json = nlohmann::json;


httplib::Server svr;

PersonRepositoryInMemory repository = PersonRepositoryInMemory();

int main(int argc, char** argv) {

    svr.Get("/hi", [](const httplib::Request &, httplib::Response &res) {
        res.set_content("Hello World!", "text/plain");
    });

    svr.Get("/people", [](const httplib::Request &, httplib::Response &res) {
        json j = repository.findAll();
        res.set_content(j.dump(), "application/json");
    });

    svr.Get(R"(/people/(\d+))", [](const httplib::Request &req, httplib::Response &res) {
        // retrieving id from url
        int id =  std::stoi(req.matches[1].str());
        // searching person by id 
        std::optional<Person> result = repository.findById(id);
        if (result) {
            json j = *result;
            res.set_content(j.dump(), "application/json");
        } else {
            throw ResponseStatusException(404, R"({ "message" : "no person with id )" + std::to_string(id) + R"( exist" })");
        }
    });

    svr.Post("/people", [](const httplib::Request &req, httplib::Response &res) {
        Person p = json::parse(req.body).get<Person>();
        p = repository.save(p);
        json j = p;
        res.set_content(j.dump(), "application/json");
    });

    svr.Put(R"(/people/(\d+))", [](const httplib::Request &req, httplib::Response &res) {
        // retrieving id from url
        int id =  std::stoi(req.matches[1].str());
        // retrieving person from request body
        Person p = json::parse(req.body).get<Person>();
        // validating id
        if (p.id == 0)
            p.id = id;
        else if (p.id != id)
            throw ResponseStatusException(400, "ids in path and body do not match");
            // updating
        repository.update(p);
    });

    svr.Delete(R"(/people/(\d+))", [](const httplib::Request &req, httplib::Response &res) {
        // retrieving id from url
        int id =  std::stoi(req.matches[1].str());
        // deleting
        repository.deleteById(id);
    });

    svr.set_exception_handler([](const auto& req, auto& res, std::exception_ptr ep) {
        try {
            std::rethrow_exception(ep);
        } catch (json::out_of_range &e) {
            res.set_content(R"({ "message": "invalid format, reason: )" + std::string(e.what()) + R"("})", "application/json");
            res.status = 400;
        } catch (ResponseStatusException &e) {
            res.set_content(R"({ "message": "invalid format, reason: )" + e.message + R"("})", "application/json");
            res.status = e.status;
        } catch (std::exception &e) {
            res.set_content(R"({ "message": "internal error, reason: )" + std::string(e.what()) + R"("})", "application/json");
            res.status = 500;
        } catch (...) {
            res.set_content(R"({ "message": "internal error, reason: unknown"})", "application/json");
            res.status = 500;
        }
    });

    svr.listen("0.0.0.0", 8080);
}