#pragma once

#include <exception>
#include <string>

class ResponseStatusException: public std::exception {

    public:

    int status;
    std::string message;

    ResponseStatusException(int status, std::string message): status(status), message(message) {};

};