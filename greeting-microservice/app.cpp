#include <array>
#include "lib/httplib.h"
#include "lib/json.hpp"
#include "models/person.h"
#include "serializers.h"

using json = nlohmann::json;

httplib::Server svr;
httplib::Client messageClient("http://localhost:8081");
httplib::Client peopleClient("http://localhost:8080");





int main(int argc, char** argv) {

    svr.Get(R"(/greet/(\d+))", [](const httplib::Request &req, httplib::Response &res) {
        // retrieving id from url
        int id =  std::stoi(req.matches[1].str());
        // retrieving message
        auto messageResponse = messageClient.Get("/messages/random");
        if (!messageResponse || messageResponse->status != 200) {
            res.status = 500;
            res.set_content(R"({ "message": "error while calling message service (status = )" + messageResponse ? std::to_string(messageResponse->status) : std::string("none") + R"() "})", 
                            "application/json");
            return;
        }
        std::string message = json::parse(messageResponse->body).get<std::string>();
        // retrieving person
        auto peopleResponse = peopleClient.Get("/people/" + std::to_string(id));
        if (peopleResponse && peopleResponse->status == 404) {
            res.status = 404;
            res.set_content(R"({ "message": "error while calling people service (status = )" + std::string(peopleResponse ? "none" : "404") + R"() ")", 
                            "application/json");
            return;
        } else if (!peopleResponse || peopleResponse->status != 200) {
            res.status = 500;
            res.set_content(R"({ "message": "error while calling people service (status = )" + std::to_string(peopleResponse->status) + R"() "})", 
                            "application/json");
            return;
        }
        Person person = json::parse(peopleResponse->body).get<Person>();
        res.status = 200;
        res.set_content(message + " " + person.name + " !", "text/plain");
    });

    svr.listen("0.0.0.0", 8082);
}