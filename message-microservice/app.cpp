#include <array>
#include "lib/httplib.h"
#include "lib/json.hpp"

using json = nlohmann::json;

httplib::Server svr;

std::array<std::string, 4> messages = {
    "Salut",
    "Hi",
    "Ciao",
    "Buenas dias"
};

int main(int argc, char** argv) {

    svr.Get("/messages/random", [](const httplib::Request &, httplib::Response &res) {
        json j = messages[std::rand()%4];
        res.set_content(j.dump(), "application/json");
    });

    svr.listen("0.0.0.0", 8081);
}